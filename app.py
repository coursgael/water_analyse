import unittest
from unittest.mock import MagicMock, patch
import json
import datetime
from app import app, Water, read_water, read_water_by_user, save_water, save_water_by_user

class TestWaterApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    @patch('app.open', create=True)
    def test_read_water(self, mock_open):
        mock_open.return_value.__enter__.return_value.read.return_value = '{"water": 10, "adding": []}'
        water = read_water()
        self.assertEqual(water.water, 10)

    @patch('app.open', create=True)
    def test_read_water_by_user(self, mock_open):
        mock_open.return_value.__enter__.return_value.read.return_value = '{"water": 10, "adding": []}'
        water = read_water_by_user('123')
        self.assertEqual(water.water, 10)

    @patch('app.open', create=True)
    def test_save_water(self, mock_open):
        water = Water(20)
        save_water(water)
        mock_open.assert_called_once_with('./water.json', 'w')
        mock_open.return_value.__enter__.return_value.write.assert_called_once_with(json.dumps(water.to_json()))

    @patch('app.open', create=True)
    def test_save_water_by_user(self, mock_open):
        water = Water(20)
        save_water_by_user(water, '123')
        mock_open.assert_called_once_with('./water123.json', 'w')
        mock_open.return_value.__enter__.return_value.write.assert_called_once_with(json.dumps(water.to_json()))

    @patch('app.read_water')
    @patch('app.datetime')
    def test_add_water(self, mock_datetime, mock_read_water):
        mock_read_water.return_value = Water(10)
        mock_datetime.datetime.now.return_value = datetime.datetime(2024, 5, 17, 12, 0, 0)
        response = self.app.get('/add_water')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"water": 20', response.data)
        self.assertIn(b'"quantity": 10', response.data)

if __name__ == '__main__':
    unittest.main()
