#!/bin/sh

pip install -r requirements.txt
coverage run -m pytest testapp.py
coverage xml